import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {


// Generate a Rendome numbers
  getRandomNumbers(){
    let rNum = [];
    for(let i=0; i<100; i++){
        let rnd =Math.floor((Math.random()*9999) + 1);
        rNum.push(rnd)
    }
    return rNum
  }


  rNum = this.getRandomNumbers().filter((x:number) => x < 2000);
  p: number = 1;
  collection: any[] = this.rNum;  

  ngOnInit() {
    console.log("-------" + this.collection)
  }

}

